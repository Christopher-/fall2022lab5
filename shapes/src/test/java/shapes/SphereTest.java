package shapes;

import static org.junit.Assert.*;

import org.junit.Test;

public class SphereTest {
    double DELTA = 0.000001;
    
    @Test
    public void testGetVolume() {
        double r = 6;
        Sphere s = new Sphere(r);
        
        double v = (4.0/3.0) * Math.PI * Math.pow(r, 3);
        assertEquals(s.getVolume(), v, DELTA);
    }

    @Test
    public void testGetSurfaceArea() {
        double r = 8;
        Sphere s = new Sphere(r);
        
        double surf = Math.PI * r*r * 4;
        assertEquals(s.getSurfaceArea(), surf, DELTA);
    }
}
