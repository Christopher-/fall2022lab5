package shapes;

import org.junit.Test;
import static org.junit.Assert.*;

public class CylinderTest {


    private final double PI = Math.PI;

    @Test
    public void testGetVolume() {
        System.out.println("getVolume");
        Cylinder instance = new Cylinder(1, 1);
        double result = instance.getVolume();
        assertEquals(PI, result, 0.0);
    }

    @Test
    public void testGetSurfaceArea() {
        System.out.println("getSurfaceArea");
        Cylinder instance = new Cylinder(1, 1);
        double result = instance.getSurfaceArea();
        assertEquals(PI*4, result, 0.0);
    }
    
}
