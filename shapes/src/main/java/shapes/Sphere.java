// Christpoher 
package shapes;

public class Sphere implements Shape3d {
    public double radius; 

    /**
     * This is a constructor. Uses radius to make a sphere
     * @param r
     */
    public Sphere (double radius) {
        this.radius = radius;
    }

    /**
     @param radius
     @returns volume
     */
    public double getVolume() {
        return (4.0/3.0) * Math.PI * Math.pow(radius, 3);
    }

    /**
     * @param radius
     * @returns surfaceArea
     */
    public double getSurfaceArea() {
        return 4 * Math.PI * Math.pow(radius, 2);
    }
    
}
