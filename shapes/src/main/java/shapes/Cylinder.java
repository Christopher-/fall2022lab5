package shapes;

public class Cylinder implements Shape3d {
    private double radius;
    private double height;
    
    /**
     * The cylinder object
     * @author Paul Alexandre Muraru
     */

    /**
     * Creates a Cylinder object which implements Shape3d
     * @param radius
     * @param height
     */
    public Cylinder(double radius, double height) {
        this.radius = radius;
        this.height = height;
    }

    /**
     * gets the radius of the cylinder and returns it
     * @return radius
     */
    public double getRadius() {
        return radius;
    }

    /**
     * Gets the height of the cylinder and returns it
     * @return height
     */
    public double getHeight() {
        return height;
    }

    /**
     * Sets the radius of the cylinder
     * @param radius
     */
    public void setRadius(double radius) {
        this.radius = radius;
    }

    /**
     * Sets the height of the cylinder
     * @param height
     */
    public void setHeight(double height) {
        this.height = height;
    }

    /**
     * getSurfaceArea calculates the surface area of the cylinder
     * and returns that value
     * 
     * @return Area
     */
    public double getSurfaceArea() {
        double area = (2 * Math.PI * radius * height + 2 * Math.PI * radius * radius);
        return area;
    }

    /**
     * getVolume calculates the volume of the cylinder
     * and returns that value
     * 
     * @return Volume
     */
    public double getVolume() {
        double volume = (Math.PI * radius * radius * height);
        return volume;
    }
}
